# Environnement de Développement Angular / Ionic

## Description du projet

### Introduction
Ce dépot va vous permettre de déployer un environnement de développement Angular et Ionic. Une fois démarré, vous pourrez commencer à coder.

### Contenu
Dans ce container, vous trouverez des images de :
- [node:latest](https://hub.docker.com/_/node).

## Prérequis
Pour utiliser ce container de manière simple, télécharger et lancer le logiciel [Docker Desktop](https://docs.docker.com/get-docker/)

Installer les extensions nécessaires dans VS Code :
- Docker
- Dev Containers

## Installation

### Installation du container
Une fois `Docker Desktop` installé, vous pouvez cloner ce dépot.
```
git clone https://gitlab.com/guillaume.nagiel/angular_ionic_env.git
```

Puis ouvrez ce dossier avec `VSCode`.

Nous allons maintenant ouvrir cette image docker en tant que `Conteneur de développement` dans `VSCode`:
- Accédez à la palette de commandes en appuyant sur Ctrl+Shift+P (ou Cmd+Shift+P sur macOS).
- Saisissez `Dev Containers: Open Folder in Container`.
- S'il vous est demandé un fichier de configuration, selectionnez `ANGULAR IONIC Dev Container`.

Le conteneur devrait s'installer avec les extentions `VSCode` pour `Angular`.


Une fois dans ce shell, créez et positionnez vous dans le dossier `project` :
```
mkdir project
cd project
```

Vous pouvez maintenant lancer la commande pour créer un projet `angular` ou `ionic`, vous la trouverez [ici](https://ionicframework.com/docs/developing/starting).
> :warning: **Pour que le projet soit accessible sur le réseau, voici la commande pour lancer le serveur : `ionic serve --external`**

Bon développement :sunglasses:

# Liste des fonctionnalités à ajouter

### Todo

### In Progress

### Done ✓

- [x] Créer le README.md  
- [x] Créer le TODO.md  