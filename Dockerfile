# Utilisez une image de base Debian Slim
FROM node:20-slim

# Installez Angular CLI et Ionic CLI globalement
RUN npm install -g @angular/cli @ionic/cli

# Définissez le répertoire de travail dans le conteneur
WORKDIR /project

# Commande par défaut pour démarrer le conteneur
CMD ["tail", "-f", "/dev/null"]